

<body>
    
        <div class="row">
            <div class="col-5">
                <h3 align="center">Connexion</h3>
                <form method="post" action="connect.treatment.php">
                    <input type="email" class="form-control" placeholder="Email" id="email_connect" name="email_connect" required>
                    <input type="password" class="form-control" placeholder="Mot de passe" id="pwd_connect" name="pwd_connect" required>
                    <center><input type="submit" class="btn_connect" name="btn_connect" value="Se connecter"></center>
                </form>
            </div>
            <div class="col-5">
                <h3 align="center">Inscription</h3>
                <form method="post" action="register.treatment.php">
                    <input type="email" class="form-control" placeholder="Email" id="email_register" name="email_register" required>
                    <input type="text" class="form-control" placeholder="Nom" id="nom_register" name="nom_register" required>
                    <input type="text" class="form-control" placeholder="Prénom" id="prenom_register" name="prenom_register" required>
                    <div class="form-group form-row">
                        <label class="col-sm-4" for="birthday_register">Date de naissance :</label>
                        <input type="date" class="form-control col-sm-8" id="birthday_register" name="birthday_register" max="2000-01-01" required>
                    </div>
                    <div class="inputSexe">
                        <label>Sexe : </label>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="homme">
                                <input type="radio" class="form-check-input" valeur="Homme" id="homme" name="sexe_register" value="Homme" checked>
                                Homme
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label" for="femme">
                                <input type="radio" class="form-check-input" valeur="Femme" id="femme" name="sexe_register" value="Femme">
                                Femme
                            </label>
                        </div>
                    </div>
                    <input type="password" class="form-control" placeholder="Mot de passe" id="pwd_register" name="pwd_register" required>
                    <center><input type="submit" class="btn_register" name="btn_register" value="S\'inscrire"></center>
                </form>
            </div>
        <div>';
    }
</body>
</html>




