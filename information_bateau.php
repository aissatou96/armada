<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>ARMADA</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.css" rel="stylesheet">
</head>

<body>
       <!-- main navigation -->
          <header>
             <!--Navbar-->
<nav class="navbar navbar-expand-lg navbar-dark primary-color">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="#">ARMADA</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="information_bateau.php">Accueil
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="programme.php">programme</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="info_pratique.php">infos pratiques</a>
      </li>

      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">accès site</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="pageaccueil.php">connexion</a>
          <a class="dropdown-item" href="pageaccueil.php">Inscription</a>
        </div>
      </li>

    </ul>
    <!-- Links -->

    <form class="form-inline">
      <div class="md-form my-0">
        <input class="form-control mr-sm-2" type="text" placeholder="Recherche" aria-label="Search">
      </div>
    </form>
  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->

          </header>

       <!-- main navigation -->

        <!-- main layout -->
          <main>
              <!-- main container -->

           <div class="container">



            <!-- Grid row -->

            <div class="row">
              <!-- grid column -->

              <div class="col-md-8">

                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
             <div class="carousel-inner">
     <div class="carousel-item active">
      <img class="d-block w-100" src="voiliers1" alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="voilier2" alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="voilier" alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
              <!-- grid column -->

              <!-- grid column -->

              <div class="col-md-12">
                <h2>ARMADA 2019-ARMADA de la liberté</h2>
                    <hr>
                    <p>
                       Large rassemblement de grands voiliers organisé à Rouen
                    </p>
                     <a href="information_bateu.php" class="btn btn-primary">Voir plus d'Informations</a>

              </div>
              <!-- grid column -->

             </div>
             <!-- Grid row -->
            </div>
              <!-- main container-->

          </main>

       <!-- main layout -->

        <!-- footer-->
          <footer>
             <!-- Footer -->
<footer class="page-footer font-small blue pt-4">

    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left">

      <!-- Grid row -->
      <div class="row">

        <!-- Grid column -->
        <div class="col-md-6 mt-md-0 mt-3">

          <!-- Content -->
          <h5 class="text-uppercase">Plus d'informations</h5><br/>
          <h1>    l'Armada du 6 au  16 Juin 2019</h1>

        </div>
        <!-- Grid column -->

        <hr class="clearfix w-100 d-md-none pb-3">

        <!-- Grid column -->
        <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Invités</h5>

            <ul class="list-unstyled">
              <li>
                <a href="#!">France</a>
              </li>
              <li>
                <a href="#!">Espagne</a>
              </li>
              <li>
                <a href="#!">Portugal</a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">

            <!-- Links -->
            <h5 class="text-uppercase">Armada en image</h5>

            <ul class="list-unstyled">
                <a href="#!">Armada 2016</a>
              </li>
              <li>
                <a href="#!">Armada 2017</a>
              </li>
              <li>
                <a href="#!">Armada 2018  </a>
              </li>
            </ul>

          </div>
          <!-- Grid column -->

      </div>
      <!-- Grid row -->

    </div>
    <!-- Footer Links -->

  </footer>
  <!-- Footer -->

          </footer>

       <!-- footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
</body>

</html>